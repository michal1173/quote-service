CREATE TABLE author
(
    id        bigserial PRIMARY KEY,
    name      VARCHAR(40)  NOT NULL,
    surname   VARCHAR(40)  NOT NULL,
    UNIQUE (name, surname),
    CONSTRAINT check_min_name_length CHECK (char_length (name) >= 3),
    CONSTRAINT check_min_surname_length CHECK (char_length(surname) >= 3)
);

CREATE TABLE quote_content
(
    id        bigserial PRIMARY KEY,
    body   VARCHAR(512)  NOT NULL,
    author_id bigserial NOT NULL REFERENCES author(id),
    CONSTRAINT check_min_content_length CHECK (char_length(body) >= 10)
);
