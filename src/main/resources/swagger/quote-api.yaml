openapi: 3.0.0
info:
  title: Quote Service
  description: Simple API for performing CRUD operations on celebrities' quotes
  version: 0.0.1
servers:
  - url: http://localhost:8080/api
    description: Local server with default spring boot port
paths:
  /quote:
    get:
      parameters:
        - in: query
          name: page
          schema:
            type: integer
            minimum: 0
          required: true
          description: number of page
        - in: query
          name: limit
          schema:
            type: integer
            minimum: 1
            maximum: 100
          required: true
          description: limit of quotes per page
      tags:
        - Quote
      operationId: getAllQuotes
      responses:
        200:
          $ref: '#/components/responses/quotesWrapper'
        400:
          $ref: '#/components/responses/error'
    post:
      tags:
        - Quote
      operationId: addNewQuote
      requestBody:
        $ref: '#/components/requestBodies/newQuoteRequest'
      responses:
        200:
          $ref: '#/components/responses/newQuoteResponse'
        400:
          $ref: '#/components/responses/error'
  /quote/{id}:
    put:
      tags:
        - Quote
      operationId: updateQuote
      parameters:
        - in: path
          schema:
            type: integer
            format: int64
            default: 1
          name: id
          example: 1
          required: true
          description: id of a quote we want to update
      requestBody:
        $ref: '#/components/requestBodies/updateQuoteRequest'
      responses:
        200:
          $ref: '#/components/responses/updateQuoteResponse'
        400:
          $ref: '#/components/responses/error'
    delete:
      tags:
        - Quote
      operationId: deleteQuote
      parameters:
        - in: path
          schema:
            type: integer
            format: int64
          name: id
          example: 1
          required: true
          description: id of a quote we want to update
      responses:
        204:
          description: Quote has been deleted succesfully
        400:
          $ref: '#/components/responses/error'
components:
  requestBodies:
    newQuoteRequest:
      description: Object containting name and surname of author and quote body
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/BasicQuote'
    updateQuoteRequest:
      description: Author and quote content to update
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/BasicQuote'
  responses:
    quotesWrapper:
      description: Object containing list of all quotes and their authors
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/QuoteListWrapper'
    newQuoteResponse:
      description: All essential quote's informations enhanced by identifiers of author and quote
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Quote'
    updateQuoteResponse:
      description: Updated object
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Quote'
    error:
      description: Found an error
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
  schemas:
    BasicAuthor:
      type: object
      properties:
        name:
          type: string
          example: Adam
          minLength: 3
          maxLength: 40
        surname:
          type: string
          example: Mickiewicz
          minLength: 3
          maxLength: 40
      required:
        - name
        - surname
    Author:
      type: object
      allOf:
        - $ref: '#/components/schemas/BasicAuthor'
        - type: object
          properties:
            id:
              type: number
              example: 1
          required:
            - id
            - name
            - surname
    BasicQuoteContent:
      type: object
      properties:
        body:
          type: string
          example: A famous quote
          minLength: 10
          maxLength: 512
      required:
        - body
    QuoteContent:
      type: object
      allOf:
        - $ref: '#/components/schemas/BasicQuoteContent'
        - type: object
          properties:
            id:
              type: number
              example: 1
          required:
            - id
    BasicQuote:
      type: object
      required:
        - author
        - content
      properties:
        author:
          $ref: '#/components/schemas/BasicAuthor'
        content:
          $ref: '#/components/schemas/BasicQuoteContent'
    Quote:
      type: object
      required:
        - author
        - content
      properties:
        author:
          $ref: '#/components/schemas/Author'
        content:
          $ref: '#/components/schemas/QuoteContent'
    QuoteListWrapper:
      type: object
      properties:
        quotes:
          type: array
          items:
            $ref: '#/components/schemas/Quote'
    Error:
      type: object
      required:
        - code
        - message
      properties:
        code:
          type: integer
          description: HTTP response error code
          example: 400
        message:
          type: string
          description: General message of what happened wrong
          example: DATA_NOT_FOUND
        errors:
          type: array
          items:
            type: string
          description: List of suberrors (if error happened due to violation of constrains)
          example: "author.nam must be shorter than 40 characters"
