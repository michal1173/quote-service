package com.decerto.michalrubaj.quoteservice.api.mapper;

import com.decerto.michalrubaj.quoteservice.data.BasicQuote;
import com.decerto.michalrubaj.quoteservice.data.BasicQuoteDto;
import com.decerto.michalrubaj.quoteservice.data.Quote;
import com.decerto.michalrubaj.quoteservice.data.QuoteDto;
import org.mapstruct.Mapper;

@Mapper
public interface QuoteApiMapper {
    BasicQuote toBasicQuote(BasicQuoteDto basicQuoteDto);
    QuoteDto toQuoteDto(Quote quote);
}
