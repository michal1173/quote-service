package com.decerto.michalrubaj.quoteservice.api.controller;

import com.decerto.michalrubaj.quoteservice.api.QuoteApi;
import com.decerto.michalrubaj.quoteservice.data.BasicQuoteDto;
import com.decerto.michalrubaj.quoteservice.data.Quote;
import com.decerto.michalrubaj.quoteservice.data.QuoteDto;
import com.decerto.michalrubaj.quoteservice.data.QuoteListWrapperDto;

import com.decerto.michalrubaj.quoteservice.api.mapper.QuoteApiMapper;
import com.decerto.michalrubaj.quoteservice.exception.InvalidDataException;
import com.decerto.michalrubaj.quoteservice.service.QuoteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class QuoteController implements QuoteApi {

    private final QuoteApiMapper quoteApiMapper;
    private final QuoteService quoteService;

    @Override
    public ResponseEntity<QuoteListWrapperDto> getAllQuotes(Integer page, Integer limit) {
         QuoteListWrapperDto response = new QuoteListWrapperDto();
         response.setQuotes(quoteService.getPaginatedQuotes(page, limit)
                                        .stream()
                                        .map(quoteApiMapper::toQuoteDto)
                                        .collect(Collectors.toList()));
         return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<QuoteDto> addNewQuote(BasicQuoteDto body) {
        return Optional.ofNullable(body)
                       .map(quoteApiMapper::toBasicQuote)
                       .map(quoteService::addNewQuote)
                       .map(quoteApiMapper::toQuoteDto)
                       .map(ResponseEntity::ok)
                       .orElseThrow(InvalidDataException::new);
    }

    @Override
    public ResponseEntity<QuoteDto> updateQuote(Long id, BasicQuoteDto body) {
        final Quote updatedQuote = quoteService.updateQuote(id, quoteApiMapper.toBasicQuote(body));
        return ResponseEntity.ok(quoteApiMapper.toQuoteDto(updatedQuote));
    }

    @Override
    public ResponseEntity<Void> deleteQuote(Long id) {
        quoteService.deleteQuote(id);
        return ResponseEntity
                .noContent()
                .build();
    }
}
