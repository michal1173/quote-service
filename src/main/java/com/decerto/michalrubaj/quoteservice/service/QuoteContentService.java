package com.decerto.michalrubaj.quoteservice.service;

import com.decerto.michalrubaj.quoteservice.data.BasicQuoteContent;
import com.decerto.michalrubaj.quoteservice.data.Quote;
import com.decerto.michalrubaj.quoteservice.data.QuoteContent;
import com.decerto.michalrubaj.quoteservice.entity.AuthorModel;
import com.decerto.michalrubaj.quoteservice.entity.QuoteContentModel;
import com.decerto.michalrubaj.quoteservice.entity.mapper.QuoteContentMapper;
import com.decerto.michalrubaj.quoteservice.exception.DataNotFoundException;
import com.decerto.michalrubaj.quoteservice.repository.AuthorRepository;
import com.decerto.michalrubaj.quoteservice.repository.QuoteContentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class QuoteContentService {

    private final QuoteContentRepository quoteContentRepository;
    private final QuoteContentMapper quoteContentMapper;
    private final AuthorRepository authorRepository;

    public QuoteContentModel createNewQuoteContent(final AuthorModel quoteAuthor,
                                                   final BasicQuoteContent basicQuoteContent) {
        QuoteContentModel newQuoteContent = quoteContentMapper.toQuoteContentModel(basicQuoteContent);
        newQuoteContent.setAuthorModel(quoteAuthor);

        return quoteContentRepository.save(newQuoteContent);
    }

    public QuoteContentModel getQuoteContent(final long quoteId) {
        return quoteContentRepository
                .findOneById(quoteId)
                .orElseThrow(DataNotFoundException::new);
    }

    public void deleteQuote(final long quoteId) {
        QuoteContentModel quoteContentModel = getQuoteContent(quoteId);
        quoteContentRepository.delete(quoteContentModel);
        if(quoteContentModel.getAuthorModel().getQuotes().isEmpty()) {
            authorRepository.delete(quoteContentModel.getAuthorModel());
        }
    }

    public List<QuoteContentModel> getPaginatedQuotes(Integer pageNumber, Integer pageSize) {
        Pageable paging = PageRequest.of(pageNumber, pageSize);
        Page<QuoteContentModel> quoteList = quoteContentRepository.findAll(paging);
        return quoteList.toList();
    }

}
