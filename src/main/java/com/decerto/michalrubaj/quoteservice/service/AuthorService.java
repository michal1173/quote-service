package com.decerto.michalrubaj.quoteservice.service;

import com.decerto.michalrubaj.quoteservice.data.BasicAuthor;
import com.decerto.michalrubaj.quoteservice.entity.AuthorModel;
import com.decerto.michalrubaj.quoteservice.entity.mapper.AuthorMapper;
import com.decerto.michalrubaj.quoteservice.repository.AuthorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
@RequiredArgsConstructor
public class AuthorService {

    private final AuthorRepository authorRepository;
    private final AuthorMapper authorMapper;

    public AuthorModel getOrCreateIfNotExists(BasicAuthor author) {
        final Optional<AuthorModel> authorDatabase = authorRepository.findByNameAndSurname(author.getName(), author.getSurname());
        return authorDatabase.orElseGet(() -> authorRepository.save(authorMapper.fromBasicAuthor(author)));
    }
}
