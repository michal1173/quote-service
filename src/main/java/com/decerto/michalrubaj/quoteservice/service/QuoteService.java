package com.decerto.michalrubaj.quoteservice.service;

import com.decerto.michalrubaj.quoteservice.data.BasicAuthor;
import com.decerto.michalrubaj.quoteservice.data.BasicQuote;
import com.decerto.michalrubaj.quoteservice.data.Quote;

import com.decerto.michalrubaj.quoteservice.entity.AuthorModel;
import com.decerto.michalrubaj.quoteservice.entity.QuoteContentModel;
import com.decerto.michalrubaj.quoteservice.entity.mapper.AuthorMapper;
import com.decerto.michalrubaj.quoteservice.entity.mapper.QuoteContentMapper;
import com.decerto.michalrubaj.quoteservice.exception.InvalidDataException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static com.decerto.michalrubaj.quoteservice.exception.enums.ErrorCodeEnum.PAGINATION_OUT_OF_RANGE;


@Getter
@Service
@RequiredArgsConstructor
public class QuoteService {

    private final QuoteContentService quoteContentService;
    private final AuthorService authorService;
    private final AuthorMapper authorMapper;
    private final QuoteContentMapper quoteContentMapper;

    @Transactional
    public Quote addNewQuote(final BasicQuote basicQuote) {
        AuthorModel author = authorService.getOrCreateIfNotExists(basicQuote.getAuthor());
        QuoteContentModel content = quoteContentService.createNewQuoteContent(author, basicQuote.getContent());
        return Quote.builder()
                    .author(authorMapper.fromAuthorModel(author))
                    .content(quoteContentMapper.toQuoteContent(content))
                    .build();
    }

    @Transactional
    public Quote updateQuote(Long quoteId, BasicQuote quoteToUpdate) {
        QuoteContentModel savedQuotedModel = quoteContentService.getQuoteContent(quoteId);
        AuthorModel savedAuthor = savedQuotedModel.getAuthorModel();
        final BasicAuthor authorToUpdate = quoteToUpdate.getAuthor();

        boolean isAuthorTheSame = savedAuthor.getName().equals(authorToUpdate.getName()) && savedAuthor.getSurname().equals(authorToUpdate.getSurname());
        boolean isContentTheSame = savedQuotedModel.getBody().equals(quoteToUpdate.getContent().getBody());

        if(!isAuthorTheSame) {
            AuthorModel author = authorService.getOrCreateIfNotExists(authorToUpdate);
            savedQuotedModel.setAuthorModel(author);
        }
        if(!isContentTheSame) {
            savedQuotedModel.setBody(quoteToUpdate.getContent().getBody());
        }
        return Quote.builder()
                    .author(authorMapper.fromAuthorModel(savedQuotedModel.getAuthorModel()))
                    .content(quoteContentMapper.toQuoteContent(savedQuotedModel))
                    .build();
    }

    public void deleteQuote(Long quoteId) {
        quoteContentService.deleteQuote(quoteId);
    }

    public List<Quote> getPaginatedQuotes(Integer pageNumber, Integer pageSize) {
        if(!isPaginationValid(pageNumber, pageSize)) {
            throw new InvalidDataException(PAGINATION_OUT_OF_RANGE);
        }
        return quoteContentService.getPaginatedQuotes(pageNumber, pageSize)
                                  .stream()
                                  .map(x -> Quote.builder()
                                                   .author(authorMapper.fromAuthorModel(x.getAuthorModel()))
                                                   .content(quoteContentMapper.toQuoteContent(x))
                                                   .build())
                                  .collect(Collectors.toList());
    }

    private boolean isPaginationValid(Integer pageNumber, Integer pageSize) {
        return (pageNumber >= 0) && (pageSize >= 1 && pageSize <= 100);
    }
}
