package com.decerto.michalrubaj.quoteservice.entity.mapper;

import com.decerto.michalrubaj.quoteservice.data.Author;
import com.decerto.michalrubaj.quoteservice.data.BasicAuthor;
import com.decerto.michalrubaj.quoteservice.entity.AuthorModel;
import org.mapstruct.Mapper;


@Mapper
public interface AuthorMapper {
    AuthorModel fromBasicAuthor(BasicAuthor basicAuthor);
    Author fromAuthorModel(AuthorModel authorModel);
}
