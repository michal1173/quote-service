package com.decerto.michalrubaj.quoteservice.entity.mapper;

import com.decerto.michalrubaj.quoteservice.data.BasicQuoteContent;
import com.decerto.michalrubaj.quoteservice.data.QuoteContent;
import com.decerto.michalrubaj.quoteservice.entity.QuoteContentModel;
import org.mapstruct.Mapper;

@Mapper
public interface QuoteContentMapper {
    QuoteContentModel toQuoteContentModel(BasicQuoteContent basicQuoteContent);
    QuoteContent toQuoteContent(QuoteContentModel quoteContentModel);
}
