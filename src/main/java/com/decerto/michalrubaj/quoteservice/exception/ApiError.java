package com.decerto.michalrubaj.quoteservice.exception;

import com.decerto.michalrubaj.quoteservice.exception.enums.ErrorCodeEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import java.util.List;
import java.util.stream.Collectors;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiError {
    private int code;
    private String message;
    private List<String> errors;

    private ApiError(int code, String message, List<String> errors) {
        this.code = code;
        this.message = message;
        this.errors = errors;
    }

    public static ApiError fromFieldError(HttpStatus statusCode, ErrorCodeEnum errorCodeEnum, List<FieldError> fieldErrors) {
       return new ApiError(statusCode.value(),
                           errorCodeEnum.name(),
                           formatErrors(fieldErrors));
    }

    public static ApiError fromException(HttpStatus httpStatus, String message) {
        return new ApiError(httpStatus.value(), message, null);
    }
    public static ApiError fromException(HttpStatus httpStatus, ErrorCodeEnum errorCodeEnum) {
        return new ApiError(httpStatus.value(), errorCodeEnum.name(), null);
    }

    private static List<String> formatErrors(List<FieldError> fieldErrors) {
        return fieldErrors.stream()
                   .map(error -> String.format("%s : %s", error.getField(), error.getDefaultMessage()))
                   .collect(Collectors.toList());
    }
}
