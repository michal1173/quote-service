package com.decerto.michalrubaj.quoteservice.exception;

import lombok.Getter;

@Getter
public abstract class RestException extends RuntimeException {
    public RestException(String message) {
        super(message);
    }
    public RestException() {
        super();
    }
}
