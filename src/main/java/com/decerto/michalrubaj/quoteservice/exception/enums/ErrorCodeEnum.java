package com.decerto.michalrubaj.quoteservice.exception.enums;

public enum ErrorCodeEnum {
    INPUT_DATA_INVALID,
    UNKNOWN_EXCEPTION,
    DATA_NOT_FOUND,
    PAGINATION_OUT_OF_RANGE
}
