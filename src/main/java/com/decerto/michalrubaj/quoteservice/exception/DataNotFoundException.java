package com.decerto.michalrubaj.quoteservice.exception;

import com.decerto.michalrubaj.quoteservice.exception.enums.ErrorCodeEnum;


public class DataNotFoundException extends RestException {
    public DataNotFoundException(String message) {
        super(message);
    }
    public DataNotFoundException() {
        super(ErrorCodeEnum.DATA_NOT_FOUND.name());
    }
}
