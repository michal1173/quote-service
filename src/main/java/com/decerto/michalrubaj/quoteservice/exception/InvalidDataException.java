package com.decerto.michalrubaj.quoteservice.exception;

import com.decerto.michalrubaj.quoteservice.exception.enums.ErrorCodeEnum;
import lombok.Getter;

@Getter
public class InvalidDataException extends RestException {

    public InvalidDataException(String message) {
        super(message);
    }
    public InvalidDataException(ErrorCodeEnum errorEnum) {
        this(errorEnum.name());
    }

    public InvalidDataException() {
       this(ErrorCodeEnum.INPUT_DATA_INVALID.name());
    }
}
