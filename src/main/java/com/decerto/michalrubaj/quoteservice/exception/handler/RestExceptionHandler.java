package com.decerto.michalrubaj.quoteservice.exception.handler;

import com.decerto.michalrubaj.quoteservice.exception.ApiError;
import com.decerto.michalrubaj.quoteservice.exception.DataNotFoundException;
import com.decerto.michalrubaj.quoteservice.exception.InvalidDataException;
import com.decerto.michalrubaj.quoteservice.exception.RestException;
import com.decerto.michalrubaj.quoteservice.exception.enums.ErrorCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {
            InvalidDataException.class,
            DataNotFoundException.class
    })
    public ResponseEntity<ApiError> handleApiException(RestException exception) {
        log.error("Exception caught: ", exception);
        ApiError errorResponse = ApiError.fromException(HttpStatus.BAD_REQUEST, exception.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        log.error("Exception caught: ", ex);
        ApiError errorResponse = ApiError.fromFieldError(status,
                                                         ErrorCodeEnum.INPUT_DATA_INVALID,
                                                         ex.getBindingResult().getFieldErrors());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception ex) {
        log.error("Exception caught: ", ex);
        ApiError errorResponse = ApiError.fromException(HttpStatus.BAD_REQUEST, ErrorCodeEnum.UNKNOWN_EXCEPTION);
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(errorResponse);
    }
}
