package com.decerto.michalrubaj.quoteservice.data;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BasicQuote {
    private BasicAuthor author;
    private BasicQuoteContent content;
}
