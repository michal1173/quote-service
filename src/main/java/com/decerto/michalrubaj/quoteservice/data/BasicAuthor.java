package com.decerto.michalrubaj.quoteservice.data;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class BasicAuthor {
    private String name;
    private String surname;
}
