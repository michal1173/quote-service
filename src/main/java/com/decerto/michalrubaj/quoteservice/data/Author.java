package com.decerto.michalrubaj.quoteservice.data;

import lombok.Data;

@Data
public class Author {
    private Long id;
    private String name;
    private String surname;
}
