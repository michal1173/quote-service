package com.decerto.michalrubaj.quoteservice.data;

import lombok.Data;


@Data
public class BasicQuoteContent {
    private String body;
}
