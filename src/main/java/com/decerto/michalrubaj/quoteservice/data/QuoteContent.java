package com.decerto.michalrubaj.quoteservice.data;

import lombok.Data;

@Data
public class QuoteContent {
    private Long id;
    private String body;
}
