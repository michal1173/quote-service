package com.decerto.michalrubaj.quoteservice.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Quote {
    private Author author;
    private QuoteContent content;
}
