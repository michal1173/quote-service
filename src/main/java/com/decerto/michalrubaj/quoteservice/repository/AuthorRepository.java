package com.decerto.michalrubaj.quoteservice.repository;

import com.decerto.michalrubaj.quoteservice.entity.AuthorModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorRepository extends JpaRepository<AuthorModel, Long> {

    Optional<AuthorModel> findByNameAndSurname(String name, String surname);
}
