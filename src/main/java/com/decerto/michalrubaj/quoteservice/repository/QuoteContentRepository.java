package com.decerto.michalrubaj.quoteservice.repository;

import com.decerto.michalrubaj.quoteservice.entity.QuoteContentModel;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface QuoteContentRepository extends JpaRepository<QuoteContentModel, Long>,
                                                PagingAndSortingRepository<QuoteContentModel, Long> {

    Optional<QuoteContentModel> findOneById(Long id);

}
