package com.decerto.michalrubaj.quoteservice;

public class JsonResourcesPath {
    public static final String REQUESTS = "/requests/";
    public static final String RESPONSES = "/responses/";
}
