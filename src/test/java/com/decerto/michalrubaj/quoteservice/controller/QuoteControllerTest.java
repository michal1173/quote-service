package com.decerto.michalrubaj.quoteservice.controller;

import com.decerto.michalrubaj.quoteservice.api.controller.QuoteController;
import com.decerto.michalrubaj.quoteservice.api.mapper.QuoteApiMapper;
import com.decerto.michalrubaj.quoteservice.data.AuthorDto;
import com.decerto.michalrubaj.quoteservice.data.BasicQuote;
import com.decerto.michalrubaj.quoteservice.data.Quote;
import com.decerto.michalrubaj.quoteservice.data.QuoteContentDto;
import com.decerto.michalrubaj.quoteservice.data.QuoteDto;
import com.decerto.michalrubaj.quoteservice.exception.DataNotFoundException;
import com.decerto.michalrubaj.quoteservice.exception.InvalidDataException;
import com.decerto.michalrubaj.quoteservice.service.QuoteService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.org.apache.commons.io.IOUtils;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;

import static com.decerto.michalrubaj.quoteservice.exception.enums.ErrorCodeEnum.PAGINATION_OUT_OF_RANGE;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.AdditionalMatchers.gt;
import static com.decerto.michalrubaj.quoteservice.JsonResourcesPath.REQUESTS;
import static com.decerto.michalrubaj.quoteservice.JsonResourcesPath.RESPONSES;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.mockito.AdditionalMatchers.leq;
import static org.mockito.AdditionalMatchers.lt;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.intThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(QuoteController.class)
public class QuoteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private QuoteApiMapper quoteApiMapper;

    @MockBean
    private QuoteService quoteService;

    @Test
    public void addNewQuote_WhenAuthorNameShorterThanThreeChars_ThenBadRequest() throws Exception {
        final String request = IOUtils.toString(this.getClass().getResourceAsStream(REQUESTS + "author_short_name.json"),
                                                Charset.defaultCharset());
        final String response = IOUtils.toString(this.getClass().getResourceAsStream(RESPONSES + "author_short_name.json"),
                                                 Charset.defaultCharset());

        mockMvc.perform(post("/quote")
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(response));
    }

    @Test
    public void addNewQuote_WhenAuthorSurnameShorterThanThreeChars_ThenBadRequest() throws Exception {
        final String request = IOUtils.toString(this.getClass().getResourceAsStream(REQUESTS + "author_short_surname.json"),
                                                Charset.defaultCharset());
        final String response = IOUtils.toString(this.getClass().getResourceAsStream(RESPONSES + "author_short_surname.json"),
                                                 Charset.defaultCharset());

        mockMvc.perform(post("/quote")
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(response));
    }

    @Test
    public void addNewQuote_WhenAuthorNull_ThenBadRequest() throws Exception {
        final String request =
                IOUtils.toString(this.getClass().getResourceAsStream(REQUESTS + "author_null.json"), Charset.defaultCharset());
        final String response =
                IOUtils.toString(this.getClass().getResourceAsStream(RESPONSES + "author_null.json"), Charset.defaultCharset());

        mockMvc.perform(post("/quote")
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(response));
    }

    @Test
    public void addNewQuote_WhenAuthorNameAndSurnameLongerThan40Chars_ThenBadRequest() throws Exception {
        final String request = IOUtils.toString(this.getClass().getResourceAsStream(REQUESTS + "author_name_and_surname_long.json"),
                                                Charset.defaultCharset());
        final String response =
                IOUtils.toString(this.getClass().getResourceAsStream(RESPONSES + "author_name_and_surname_long.json"),
                                 Charset.defaultCharset());

        mockMvc.perform(post("/quote")
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(response));
    }

    @Test
    public void addNewQuote_WhenQuoteContentNull_ThenBadRequest() throws Exception {
        final String request = IOUtils.toString(this.getClass().getResourceAsStream(REQUESTS + "quote_content_null.json"),
                                                Charset.defaultCharset());
        final String response = IOUtils.toString(this.getClass().getResourceAsStream(RESPONSES + "quote_content_null.json"),
                                                 Charset.defaultCharset());

        mockMvc.perform(post("/quote")
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(response));
    }

    @Test
    public void addNewQuote_WhenProperAuthorAndQuoteContent_ThenOk() throws Exception {

        String request = IOUtils.toString(this.getClass().getResourceAsStream(REQUESTS + "proper_author_and_quote_content.json"),
                                          Charset.defaultCharset());

        QuoteDto response = new QuoteDto();
        AuthorDto authorDto = new AuthorDto();
        authorDto.setId(BigDecimal.ONE);
        authorDto.setName("Adam");
        authorDto.setSurname("Mickiewicz");
        QuoteContentDto quoteContentDto = new QuoteContentDto();
        quoteContentDto.setId(BigDecimal.ONE);
        quoteContentDto.setBody("A famous quote");
        response.setAuthor(authorDto);
        response.setContent(quoteContentDto);

        when(quoteApiMapper.toBasicQuote(any())).thenReturn(new BasicQuote());
        when(quoteService.addNewQuote(new BasicQuote())).thenReturn(new Quote());
        when(quoteApiMapper.toQuoteDto(new Quote())).thenReturn(response);

        mockMvc.perform(post("/quote")
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.author.name", is("Adam")))
               .andExpect(jsonPath("$.author.surname", is("Mickiewicz")))
               .andExpect(jsonPath("$.author.id", is(1)))
               .andExpect(jsonPath("$.content.id", is(1)))
               .andExpect(jsonPath("$.content.body", is("A famous quote")));
    }

    @Test
    public void updateQuote_WhenAuthorNameShorterThanThreeChars_ThenBadRequest() throws Exception {
        final String request = IOUtils.toString(this.getClass().getResourceAsStream(REQUESTS + "author_short_name.json"),
                                                Charset.defaultCharset());
        final String response = IOUtils.toString(this.getClass().getResourceAsStream(RESPONSES + "author_short_name.json"),
                                                 Charset.defaultCharset());

        mockMvc.perform(put("/quote/1")
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(response));
    }

    @Test
    public void updateQuote_WhenAuthorSurnameShorterThanThreeChars_ThenBadRequest() throws Exception {
        final String request = IOUtils.toString(this.getClass().getResourceAsStream(REQUESTS + "author_short_surname.json"),
                                                Charset.defaultCharset());
        final String response = IOUtils.toString(this.getClass().getResourceAsStream(RESPONSES + "author_short_surname.json"),
                                                 Charset.defaultCharset());

        mockMvc.perform(put("/quote/1")
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(response));
    }

    @Test
    public void updateQuote_WhenAuthorNull_ThenBadRequest() throws Exception {
        final String request =
                IOUtils.toString(this.getClass().getResourceAsStream(REQUESTS + "author_null.json"), Charset.defaultCharset());
        final String response =
                IOUtils.toString(this.getClass().getResourceAsStream(RESPONSES + "author_null.json"), Charset.defaultCharset());

        mockMvc.perform(put("/quote/1")
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(response));
    }

    @Test
    public void updateQuote_WhenAuthorNameAndSurnameLongerThan40Chars_ThenBadRequest() throws Exception {
        final String request = IOUtils.toString(this.getClass().getResourceAsStream(REQUESTS + "author_name_and_surname_long.json"),
                                                Charset.defaultCharset());
        final String response =
                IOUtils.toString(this.getClass().getResourceAsStream(RESPONSES + "author_name_and_surname_long.json"),
                                 Charset.defaultCharset());

        mockMvc.perform(put("/quote/1")
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(response));
    }

    @Test
    public void updateQuote_WhenQuoteContentNull_ThenBadRequest() throws Exception {
        final String request = IOUtils.toString(this.getClass().getResourceAsStream(REQUESTS + "quote_content_null.json"),
                                                Charset.defaultCharset());
        final String response = IOUtils.toString(this.getClass().getResourceAsStream(RESPONSES + "quote_content_null.json"),
                                                 Charset.defaultCharset());

        mockMvc.perform(put("/quote/1")
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(response));
    }

    @Test
    public void updateQuote_WhenProperAuthorAndQuoteContent_ThenOk() throws Exception {

        String request = IOUtils.toString(this.getClass().getResourceAsStream(REQUESTS + "proper_author_and_quote_content.json"),
                                          Charset.defaultCharset());

        QuoteDto response = new QuoteDto();
        AuthorDto authorDto = new AuthorDto();
        authorDto.setId(BigDecimal.ONE);
        authorDto.setName("Jan");
        authorDto.setSurname("Kowalski");
        QuoteContentDto quoteContentDto = new QuoteContentDto();
        quoteContentDto.setId(BigDecimal.ONE);
        quoteContentDto.setBody("A slightly less famous quote");
        response.setAuthor(authorDto);
        response.setContent(quoteContentDto);

        when(quoteApiMapper.toBasicQuote(any())).thenReturn(new BasicQuote());
        when(quoteService.updateQuote(1L, new BasicQuote())).thenReturn(new Quote());
        when(quoteApiMapper.toQuoteDto(new Quote())).thenReturn(response);

        mockMvc.perform(put("/quote/1")
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.author.name", is("Jan")))
               .andExpect(jsonPath("$.author.surname", is("Kowalski")))
               .andExpect(jsonPath("$.author.id", is(1)))
               .andExpect(jsonPath("$.content.id", is(1)))
               .andExpect(jsonPath("$.content.body", is("A slightly less famous quote")));
    }

    @Test
    public void deleteQuote_WhenWrongPath_ThenBadRequest() throws Exception {
        final String response = IOUtils.toString(this.getClass().getResourceAsStream(RESPONSES + "quote_not_found.json"),
                                                 Charset.defaultCharset());
        Long quoteId = 999L;
        doThrow(new DataNotFoundException()).when(quoteService).deleteQuote(quoteId);
        mockMvc.perform(delete("/quote/" + quoteId))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(response));
    }

    @Test
    public void deleteQuote_WhenWrongPath_ThenNoContent() throws Exception {

        Long quoteId = 1L;
        doNothing().when(quoteService).deleteQuote(quoteId);
        mockMvc.perform(delete("/quote/" + quoteId))
               .andExpect(status().isNoContent());
    }

    @Test
    public void getAllQuotes_WhenPaginationLimitMoreThan100_ThenBadRequest() throws Exception {
        final String response = IOUtils.toString(this.getClass().getResourceAsStream(RESPONSES + "pagination_out_of_range.json"),
                                                 Charset.defaultCharset());

        when(quoteService.getPaginatedQuotes(anyInt(), gt(100))).thenThrow(new InvalidDataException(PAGINATION_OUT_OF_RANGE));

        mockMvc.perform(get("/quote")
                                .param("page", "0")
                                .param("limit", "101"))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(response));
    }

    @Test
    public void getAllQuotes_WhenPaginationWithinRange_ThenBadRequest() throws Exception {

        int size = 56;
        when(quoteService.getPaginatedQuotes(anyInt(), leq(100))).thenReturn(Arrays.asList(new Quote[size]));

        mockMvc.perform(get("/quote")
                                .param("page", "0")
                                .param("limit", String.valueOf(size)))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.quotes", hasSize(size)));
    }
}
