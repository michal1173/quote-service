package com.decerto.michalrubaj.quoteservice.e2e;

import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Testcontainers
@ExtendWith(SpringExtension.class)
@ContextConfiguration(initializers = PostgresTestContainer.DockerPostgreDataSourceInitializer.class)
@TestInstance(PER_CLASS)
public abstract class PostgresTestContainer  {

    @Container
    public static PostgreSQLContainer<?> postgreDBContainer = new PostgreSQLContainer<>("postgres:11.1");

    static {
        postgreDBContainer.start();
    }

    protected static class DockerPostgreDataSourceInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        private static final String SPRING_DATASOURCE_PREFIX = "spring.datasource.";
        private static final String URL_PROPERTY = "url";
        private static final String USERNAME_PROPERTY = "username";
        private static final String PASSWORD_PROPERTY = "password";

        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            TestPropertySourceUtils.addInlinedPropertiesToEnvironment(applicationContext, getProperties());
        }

        private static String[] getProperties() {
            return new String[]{
                    buildProperty(URL_PROPERTY, postgreDBContainer.getJdbcUrl()),
                    buildProperty(USERNAME_PROPERTY, postgreDBContainer.getUsername()),
                    buildProperty(PASSWORD_PROPERTY, postgreDBContainer.getPassword())
            };
        }

        private static String buildProperty(String property, String value) {
            return SPRING_DATASOURCE_PREFIX + property + "=" + value;
        }
    }

}
