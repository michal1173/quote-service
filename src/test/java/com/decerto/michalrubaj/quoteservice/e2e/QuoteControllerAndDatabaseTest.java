package com.decerto.michalrubaj.quoteservice.e2e;


import com.decerto.michalrubaj.quoteservice.entity.AuthorModel;
import com.decerto.michalrubaj.quoteservice.entity.QuoteContentModel;
import com.decerto.michalrubaj.quoteservice.repository.AuthorRepository;
import com.decerto.michalrubaj.quoteservice.repository.QuoteContentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.org.apache.commons.io.IOUtils;

import javax.transaction.Transactional;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Optional;

import static com.decerto.michalrubaj.quoteservice.JsonResourcesPath.REQUESTS;
import static com.decerto.michalrubaj.quoteservice.JsonResourcesPath.RESPONSES;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class QuoteControllerAndDatabaseTest extends PostgresTestContainer {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private QuoteContentRepository quoteContentRepository;

    @Test
    public void addNewQuote_WhenProperAuthorAndQuoteContent_ThenOk() throws Exception {
        final String request = IOUtils.toString(this.getClass().getResourceAsStream(REQUESTS + "proper_author_and_quote_content.json"), Charset.defaultCharset());

        mockMvc.perform(post("/quote")
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.author.name", is("Adam")))
               .andExpect(jsonPath("$.author.surname", is("Mickiewicz")))
               .andExpect(jsonPath("$.author.id", notNullValue()))
               .andExpect(jsonPath("$.content.id", notNullValue()))
               .andExpect(jsonPath("$.content.body", is("A famous quote")));
    }

    @Test
    public void updateQuote_WhenProperAuthorAndQuoteContent_ThenOk() throws Exception {

        AuthorModel authorModel =  AuthorModel.builder()
                                              .name("Jan")
                                              .surname("Kowalski")
                                              .quotes(null)
                                              .build();
        final AuthorModel save = authorRepository.save(authorModel);

        QuoteContentModel quoteContentModel =  QuoteContentModel.builder()
                                                                .body("Another famous quote")
                                                                .authorModel(save)
                                                                .build();

        quoteContentRepository.save(quoteContentModel);

        final String request = IOUtils.toString(this.getClass().getResourceAsStream(REQUESTS + "proper_author_and_quote_content.json"), Charset.defaultCharset());

        mockMvc.perform(put("/quote/" + quoteContentModel.getId())
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.author.name", is("Adam")))
               .andExpect(jsonPath("$.author.surname", is("Mickiewicz")))
               .andExpect(jsonPath("$.author.id", notNullValue()))
               .andExpect(jsonPath("$.author.id", not(authorModel.getId().intValue())))
               .andExpect(jsonPath("$.content.id", is(quoteContentModel.getId().intValue())))
               .andExpect(jsonPath("$.content.body", is("A famous quote")));
    }

    @Test
    public void updateQuote_WhenQuoteIdDoesntExist_ThenBadRequest() throws Exception {

        final String request = IOUtils.toString(this.getClass().getResourceAsStream(REQUESTS + "proper_author_and_quote_content.json"), Charset.defaultCharset());
        final String response = IOUtils.toString(this.getClass().getResourceAsStream(RESPONSES + "quote_not_found.json"), Charset.defaultCharset());

        mockMvc.perform(put("/quote/9999")
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(response));
    }

    @Test
    public void deleteQuote_WhenCorrectQuoteId_ThenNoContent() throws Exception {

        AuthorModel authorModel =  AuthorModel.builder()
                                              .name("Marcin")
                                              .surname("Kowalski")
                                              .quotes(new ArrayList<>())
                                              .build();
        final AuthorModel save = authorRepository.save(authorModel);

        QuoteContentModel quoteContentModel =  QuoteContentModel.builder()
                                                                .body("a very famous quote")
                                                                .authorModel(save)
                                                                .build();

        quoteContentRepository.save(quoteContentModel);

        mockMvc.perform(delete("/quote/" + quoteContentModel.getId()))
               .andExpect(status().isNoContent());

        final Optional<AuthorModel> savedAuthor = authorRepository.findById(authorModel.getId());
        final Optional<QuoteContentModel> savedQuoteContent = quoteContentRepository.findOneById(quoteContentModel.getId());

        assertFalse(savedAuthor.isPresent());
        assertFalse(savedQuoteContent.isPresent());
    }

    @Test
    public void getAllQuote_WhenLimit_2_AndFirstPage_ThenReturnsOkAndListOfSize_2() throws Exception {

        AuthorModel authorModel =  AuthorModel.builder()
                                              .name("John")
                                              .surname("Novak")
                                              .quotes(new ArrayList<>())
                                              .build();
        final AuthorModel save = authorRepository.save(authorModel);

        QuoteContentModel quoteContentModel =  QuoteContentModel.builder()
                                                                .body("a very famous quote")
                                                                .authorModel(save)
                                                                .build();

        quoteContentRepository.save(quoteContentModel);

        QuoteContentModel secondQuote =  QuoteContentModel.builder()
                                                                .body("a very famous quote")
                                                                .authorModel(save)
                                                                .build();
        quoteContentRepository.save(secondQuote);

        int limit = 2;

        mockMvc.perform(get("/quote")
                                .param("page", "0")
                                .param("limit", String.valueOf(limit))
                       )
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.quotes", hasSize(2)));

    }

    @Test
    public void getAllQuote_WhenLimit_2_AndSecondPage_ThenReturnsOkAndListOfSize_0() throws Exception {

        AuthorModel authorModel =  AuthorModel.builder()
                                              .name("Adam")
                                              .surname("Owl")
                                              .quotes(new ArrayList<>())
                                              .build();
        final AuthorModel save = authorRepository.save(authorModel);

        QuoteContentModel quoteContentModel =  QuoteContentModel.builder()
                                                                .body("a very famous quote")
                                                                .authorModel(save)
                                                                .build();

        quoteContentRepository.save(quoteContentModel);

        QuoteContentModel secondQuote =  QuoteContentModel.builder()
                                                          .body("a very famous quote")
                                                          .authorModel(save)
                                                          .build();
        quoteContentRepository.save(secondQuote);

        int limit = 2;

        mockMvc.perform(get("/quote")
                                .param("page", "1")
                                .param("limit", String.valueOf(limit))
                       )
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.quotes", hasSize(0)));
    }

    @Test
    public void getAllQuote_WhenLimit_1_AndSecondPage_ThenReturnsOkAndListOfSize_1() throws Exception {

        AuthorModel authorModel =  AuthorModel.builder()
                                              .name("John")
                                              .surname("Smith")
                                              .quotes(new ArrayList<>())
                                              .build();
        final AuthorModel save = authorRepository.save(authorModel);

        QuoteContentModel quoteContentModel =  QuoteContentModel.builder()
                                                                .body("a very famous quote")
                                                                .authorModel(save)
                                                                .build();

        quoteContentRepository.save(quoteContentModel);

        QuoteContentModel secondQuote =  QuoteContentModel.builder()
                                                          .body("a very famous quote")
                                                          .authorModel(save)
                                                          .build();
        quoteContentRepository.save(secondQuote);

        int limit = 1;

        mockMvc.perform(get("/quote")
                                .param("page", "1")
                                .param("limit", String.valueOf(limit))
                       )
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.quotes", hasSize(1)));
    }
}
