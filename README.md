# Quote Service

Quote Service is a simple RESTFUL application design to perform CRUD operations on famous people's quotes.

## Requirements

* Java 8
* Maven 3.6.0
* Postgres 12.5
* Docker 20.10.1 (only for running tests)

## Installation

```bash
git clone https://gitlab.com/michal1173/quote-service.git

# this is an essential step as it generates classes, 
# it by default runs the tests, so if you don't have Docker installed add flag -DskipTests
# be sure to run this command in the same directory as pom.xml

mvn package

# now you need to set environmental variables,
# commands may differ depending on which OS you are running the app
# but for Linux it is as follows

export DB_HOST = {your database host}
export DB_PORT = {your database port, only if you run postgres on a different port than 5432}
export DB_NAME = {your database name, create an empty db before running the app}
export DB_USER = {your database username}
export DB_PASSWORD = {your database password} 


# to run the application

mvn spring-boot:run

```

## API Documentation

Whole API documentation can be found:

* as soon as the application is running on the endpoint **/api/swagger-ui.html**
* on SwaggerHub in the link [here](https://app.swaggerhub.com/apis/michalrubaj/Quote_Service/1.0.0)

## License
[MIT](https://choosealicense.com/licenses/mit/)
